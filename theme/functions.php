<?php

function theme_assets() {    
    wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
    wp_enqueue_style('style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'theme_assets');


function load_js() {
    wp_enqueue_script('jquery');    
    wp_enqueue_script('popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js');
    wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', 'jquery', false, true);
    wp_register_script('app', get_template_directory_uri() . '/js/app.js', array(), false, true);
    wp_enqueue_script('app');
}

add_action('wp_enqueue_scripts', 'load_js');
?>