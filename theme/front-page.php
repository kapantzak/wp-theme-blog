<?php get_header();?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="post-card card mb-3">
                    <img src="https://via.placeholder.com/1000x300" class="card-img-top" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">
                            <small>By Admin</small>
                            <small>at 12/12/2019</small>
                        </p>
                        <p class="card-text">
                            This is a wider card with supporting text below as a natural
                            lead-in to additional content. This content is a little bit
                            longer.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col mb-4">
            <div class="post-card card h-100">
                <img src="https://via.placeholder.com/650" class="card-img-top" alt="..." />
                <div class="card-body">
                    <h5 class="card-title">This is a post title</h5>
                    <p class="card-text">
                        <small>By Admin</small>
                        <small>at 12/12/2019</small>
                    </p>
                    <p class="card-text">
                        This is a longer card with supporting text below as a natural
                        lead-in to additional content. This content is a little bit
                        longer.
                    </p>
                </div>
            </div>
            </div>

            <div class="col mb-4">
            <div class="post-card card h-100">
                <img src="https://via.placeholder.com/650" class="card-img-top" alt="..." />
                <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">
                    <small>By Admin</small>
                    <small>at 12/12/2019</small>
                </p>
                <p class="card-text">This is a short card.</p>
            </div>
        </div>
        </div>

        <div class="col mb-4">
        <div class="post-card card h-100">
            <img src="https://via.placeholder.com/650" class="card-img-top" alt="..." />
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">
                    <small>By Admin</small>
                    <small>at 12/12/2019</small>
                </p>
                <p class="card-text">
                    This is a longer card with supporting text below as a natural
                    lead-in to additional content.
                </p>
            </div>
        </div>
        </div>
    </div>
</div>

<div>
    test
</div>

<div class="container">
    test
</div>

<?php get_footer();?>