<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head() ?>    
  </head>
  <body>

  <div id="header">
    <div id="logoHolder" class="text-center">
        <span>Logo</span>
    </div>
    <nav id="mainNavigation" class="nav justify-content-center">
        <a class="nav-link active" href="#">Home</a>
        <a id="btnCategoriesMenu" class="nav-link" href="#">Categories</a>
        <a class="nav-link" href="#">About</a>
    </nav>
    <div id="categoriesMenu">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-12 col-md-4">
                    <dl class="category-list">
                    <dt>Category 1</dt>
                    <dd>Category 1.1</dd>
                    <dd>Category 1.2</dd>
                    <dd>Category 1.3</dd>
                    </dl>
                </div>
                <div class="col-sm-12 col-md-4">
                    <dl class="category-list">
                    <dt>Category 1</dt>
                    <dd>Category 1.1</dd>
                    <dd>Category 1.2</dd>
                    <dd>Category 1.3</dd>
                    </dl>
                </div>
                <div class="col-sm-12 col-md-4">
                    <dl class="category-list">
                    <dt>Category 1</dt>
                    <dd>Category 1.1</dd>
                    <dd>Category 1.2</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="content">